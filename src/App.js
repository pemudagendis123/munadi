import "./App.css"
import NavigationBar from "./components/NavigationBar"
import Intro from "./components/Intro"
import Trending from "./components/Trending"
import Video from "./components/Video"
import SuperHero from "./components/Superhero"

import "./style/landingPage.css"

function App() {
  return (
    <div>
      <div className="myBG">
        <NavigationBar />
        <Intro />
      </div>

      <div className="trending">
        <Trending />
      </div>

      <div className="video">
        <Video />
      </div>

      <div className="superhero">
        <SuperHero />
      </div>
    </div>
  )
}

export default App
