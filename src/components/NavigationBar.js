import { Navbar, Container, Nav } from "react-bootstrap"

const NavigationBar = () => {
  return (
    <div>
      <Navbar variant="dark">
        <Container>
          <Navbar.Brand href="/">FILMS</Navbar.Brand>
          <Nav>
            <div class="Name">
                <input type="search" class="Name" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                <span class="Namer-" id="search-addon">
                  <i class="Name"></i>
                </span>
              </div>
            <Nav.Link href="#trending">TRENDING</Nav.Link>
            <Nav.Link href="#superhero">SUPERHERO</Nav.Link>
            <Nav.Link href="#video">TV MOVIES</Nav.Link>
            <Nav.Link href="#comen">COMEN</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </div>
  )
}

export default NavigationBar
